/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 * Clase que modela un estudiante
 *
 * @author Luis Vergel
 */
public class Estudiante implements Comparable<Estudiante> {

    private String nombre;
    private long codigo;
    private float p1, p2, p3;

    //Requisito 1
    /**
     * Constructor vacio de la clase estudiante
     */
    public Estudiante() {
    }

    /**
     * Constructor con parametros de la clase estudiante
     *
     * @param nombre Variable que almacena el nombre de un estudiante en un
     * String
     * @param codigo Variable que almacena el codigo de un estudiante en un
     * entero
     * @param p1 Variable que almacena la nota 1 de un estudiante en un float
     * @param p2 Variable que almacena la nota 2 de un estudiante en un float
     * @param p3 Variable que almacena la nota 3 de un estudiante en un float
     */
    public Estudiante(String nombre, long codigo, float p1, float p2, float p3) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }

    //Requisito 2
    /**
     * Método que obtiene el valor del atributo nombre
     *
     * @return un String que contiene el nombre del estudiante
     */
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public float getP1() {
        return p1;
    }

    public void setP1(float p1) {
        this.p1 = p1;
    }

    public float getP2() {
        return p2;
    }

    public void setP2(float p2) {
        this.p2 = p2;
    }

    public float getP3() {
        return p3;
    }

    public void setP3(float p3) {
        this.p3 = p3;
    }

    //Requisito 3
    @Override
    public String toString() {
        return "Estudiante{" + "nombre=" + nombre + ", codigo=" + codigo + ", p1=" + p1 + ", p2=" + p2 + ", p3=" + p3 + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (int) (this.codigo ^ (this.codigo >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Estudiante other = (Estudiante) obj;
        float promedioMio = this.getDefinitiva();
        float promedioOther = other.getDefinitiva();

        if (promedioMio == promedioOther) {
            return true;

        } else {
            return false;
        }
    }

    public float getDefinitiva() {
        float promedio = this.p1 * 0.3F + this.p2 * 0.3F + this.p3 * 0.4F;
        return promedio;
    }

    @Override
    public int compareTo(Estudiante o) {

        float miNota = this.getDefinitiva();
        float otroNota = o.getDefinitiva();
        float restaNota = miNota - otroNota;
        if (restaNota == 0) {
            return 0;
        } else if (restaNota < 0) {
            return -1;
        } else {
            return 1;
        }

    }

}
