/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import java.util.LinkedList;

/**
 *
 * @author madar
 */
public class ListaFraccionarios {

    private LinkedList<Fraccionario> myLista = new LinkedList();

    public ListaFraccionarios() {
    }

    public LinkedList<Fraccionario> getMyLista() {
        return myLista;
    }

    public void setMyLista(LinkedList<Fraccionario> myLista) {
        this.myLista = myLista;
    }

    @Override
    public String toString() {

        String msg = "";
        for (Fraccionario f : myLista) {

            msg += f.toString() + "\t";

        }
        return msg;
    }

    public void insertar(Fraccionario nuevo) {
        this.myLista.add(nuevo);
    }

    public Fraccionario getMenor() {
        // menor = myLista[0]
        Fraccionario menor = this.myLista.get(0);
        for (int i = 1; i < this.myLista.size(); i++) {
            Fraccionario dos = this.myLista.get(i);
            int c = menor.compareTo(dos);
            // menor - dos 
            if (c > 0) {
                menor = dos;
            }
        }
        return menor;
    }

    public Fraccionario getSuma() {
        return null;
    }

    public Fraccionario getResta() {
        return null;
    }

    public Fraccionario getMultiplicacion() {
        return null;
    }

    public Fraccionario getDivision() {
        return null;
    }

}
