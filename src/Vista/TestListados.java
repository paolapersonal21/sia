/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.Estudiante;
import Modelo.Fraccionario;
import Modelo.ListaFraccionarios;
import Modelo.ListadoEstudiante;

/**
 *
 * @author madar
 */
public class TestListados {

    public static void main(String[] args) {
        ListadoEstudiante estudiantes = new ListadoEstudiante();
        ListaFraccionarios fraccionarios = new ListaFraccionarios();

        //Adicionar elementos a esos listados:
        Estudiante uno = new Estudiante("madarme", 1, 2, 3, 4);
        Estudiante dos = new Estudiante("luis", 2, 3, 3, 4);
        Estudiante tres = new Estudiante("oriana", 3, 4, 4, 4);
        //insertar esos elementos al listado:
        estudiantes.insertar(uno);
        estudiantes.insertar(dos);
        estudiantes.insertar(tres);
        System.out.println(estudiantes.toString());

        //fraccionarios:
        Fraccionario x1 = new Fraccionario(3, 4);
        Fraccionario x2 = new Fraccionario(3, 5);
        Fraccionario x3 = new Fraccionario(13, 6);
        
        //Insertar en la lista de fraccionarios:
        fraccionarios.insertar(x1);
        fraccionarios.insertar(x2);
        fraccionarios.insertar(x3);
        System.out.println(fraccionarios.toString());
        
        //Probando métodos de los listados:
        System.out.println("Menor estudiante:"+estudiantes.getMenor());
        System.out.println("Menor Fracción:"+fraccionarios.getMenor());
        

    }
}
